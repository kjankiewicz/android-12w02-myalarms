package com.example.kjankiewicz.android_12w02_myalarms

import android.app.IntentService
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.os.Build
import android.support.annotation.RequiresApi
import android.support.v4.app.NotificationCompat
import android.support.v4.app.NotificationManagerCompat
import android.util.Log


class MyAlarmIntentService(private val context: Context) : IntentService("MyAlarmIntentService") {
    private lateinit var notificationManagerCompat: NotificationManagerCompat

    override fun onHandleIntent(intent: Intent?) {
        Log.d(TAG, "onHandleIntent")
        if (intent != null) {
            val action = intent.action
            if (ACTION_ERT == action) {
                val param1 = intent.getStringExtra(WAKEUP_TYPE)
                handleActionERT(param1)
            } else if (ACTION_RTC == action) {
                val param1 = intent.getStringExtra(WAKEUP_TYPE)
                handleActionRTC(param1)
            }
        }
    }

    private fun handleActionERT(wakeup_type: String) {
        Log.d(TAG, "handleActionERT")

        val channelId: String = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createNotificationChannel()
        } else {
            ""
        }

        val notification = NotificationCompat.Builder(this, channelId)
                .setContentTitle("ERT Alarm")
                .setContentText("WakeUp Type: $wakeup_type")
                .setSmallIcon(R.drawable.oak_foreground)
                .setAutoCancel(true).build()

        notificationManagerCompat = NotificationManagerCompat.from(context)

        notificationManagerCompat.notify(0, notification)
    }

    private fun handleActionRTC(wakeup_type: String) {
        Log.d(TAG, "handleActionRTC")

        val channelId: String = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createNotificationChannel()
        } else {
            ""
        }

        val notification = NotificationCompat.Builder(this, channelId)
                .setContentTitle("RTC Alarm")
                .setContentText("WakeUp Type: $wakeup_type")
                .setSmallIcon(R.drawable.oak_foreground)
                .setAutoCancel(true).build()

        notificationManagerCompat = NotificationManagerCompat.from(context)

        notificationManagerCompat.notify(0, notification)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChannel(): String {
        val channelId = "AlarmServiceChannel"
        val channelName = "Alarm Service Channel"
        val channel = NotificationChannel(channelId,
                channelName, NotificationManager.IMPORTANCE_DEFAULT)
        val notificationManager = getSystemService(
                Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.createNotificationChannel(channel)
        return channelId
    }

    companion object {
        const val ACTION_ERT = "com.example.kjankiewicz.android_12w02_myalarms.action.ERT"
        const val ACTION_RTC = "com.example.kjankiewicz.android_12w02_myalarms.action.RTC"

        const val WAKEUP_TYPE = "com.example.kjankiewicz.android_12w02_myalarms.extra.WAKEUP_TYPE"

        private const val TAG = "MyAlarmIntentService"
    }
}
