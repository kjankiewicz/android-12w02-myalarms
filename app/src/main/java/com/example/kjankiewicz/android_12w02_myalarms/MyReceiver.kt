package com.example.kjankiewicz.android_12w02_myalarms

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log

class MyReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        val alarmType: String = intent.getStringExtra(MyMainActivity.TYPE_OF_ALARM_parameter)
        Log.d(TAG, "onReceive")
        val intentService = Intent(context, MyAlarmIntentService::class.java)
        Log.d(TAG, "type_of_alarm: $alarmType")
        if (alarmType.contains("WAKEUP"))
            intentService.putExtra(MyAlarmIntentService.WAKEUP_TYPE, "WAKEUP")
        else
            intentService.putExtra(MyAlarmIntentService.WAKEUP_TYPE, "NOWAKEUP")
        if (alarmType.contains("ELAPSED_REALTIME"))
            intentService.action = MyAlarmIntentService.ACTION_ERT
        else
            intentService.action = MyAlarmIntentService.ACTION_RTC
        context.startService(intentService)
    }

    companion object {
        private const val TAG = "MyReceiver"
    }
}
