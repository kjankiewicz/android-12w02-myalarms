package com.example.kjankiewicz.android_12w02_myalarms

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.SystemClock
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Menu
import android.view.MenuItem

import java.util.Calendar


class MyMainActivity : AppCompatActivity() {

    private lateinit var alarmMgr: AlarmManager
    private var alarmIntent: PendingIntent? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_main)
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.my_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        alarmMgr = getSystemService(Context.ALARM_SERVICE) as AlarmManager

        val id = item.itemId
        if (id == R.id.elapsed_realtime) {
            Log.d(TAG, "ELAPSED_REALTIME")
            val intent = Intent(this, MyReceiver::class.java)
            intent.putExtra(TYPE_OF_ALARM_parameter, "ELAPSED_REALTIME")
            alarmIntent = PendingIntent.getBroadcast(this, 0, intent, 0)

            alarmMgr.setRepeating(AlarmManager.ELAPSED_REALTIME,
                    AlarmManager.INTERVAL_FIFTEEN_MINUTES,
                    AlarmManager.INTERVAL_FIFTEEN_MINUTES, alarmIntent)

            return true
        }
        if (id == R.id.elapsed_realtime_wakeup) {
            Log.d(TAG, "ELAPSED_REALTIME_WAKEUP")
            val intent = Intent(this, MyReceiver::class.java)
            intent.putExtra(TYPE_OF_ALARM_parameter, "ELAPSED_REALTIME_WAKEUP")
            alarmIntent = PendingIntent.getBroadcast(this, 0, intent, 0)

            alarmMgr.set(AlarmManager.ELAPSED_REALTIME_WAKEUP,
                    SystemClock.elapsedRealtime() + 60 * 1000, alarmIntent)
            return true
        }
        if (id == R.id.realtime_clock) {
            Log.d(TAG, "RTC")
            val intent = Intent(this, MyReceiver::class.java)
            intent.putExtra(TYPE_OF_ALARM_parameter, "RTC")
            alarmIntent = PendingIntent.getBroadcast(this, 0, intent, 0)

            val calendar = Calendar.getInstance()
            calendar.timeInMillis = System.currentTimeMillis()
            calendar.set(Calendar.HOUR_OF_DAY, 8)

            alarmMgr.setInexactRepeating(AlarmManager.RTC,
                    calendar.timeInMillis,
                    AlarmManager.INTERVAL_DAY, alarmIntent)

            return true
        }
        if (id == R.id.realtime_clock_wakeup) {
            Log.d(TAG, "RTC_WAKEUP")
            val intent = Intent(this, MyReceiver::class.java)
            intent.putExtra(TYPE_OF_ALARM_parameter, "RTC_WAKEUP")
            alarmIntent = PendingIntent.getBroadcast(this, 0, intent, 0)

            val calendar = Calendar.getInstance()
            calendar.timeInMillis = System.currentTimeMillis()
            calendar.set(Calendar.HOUR_OF_DAY, 15)
            calendar.set(Calendar.MINUTE, 30)

            alarmMgr.setRepeating(AlarmManager.RTC_WAKEUP,
                    calendar.timeInMillis,
                    (1000 * 60 * 25).toLong(), alarmIntent)

            return true
        }
        return super.onOptionsItemSelected(item)
    }

    companion object {
        private const val TAG = "MyMainActivity"

        var TYPE_OF_ALARM_parameter = "TYPE_OF_ALARM"
    }
}
